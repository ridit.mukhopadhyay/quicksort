
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int[] arr = {18,27, 50, 9, 31, 32};
		quickSort(arr, 0, arr.length -1);
		displayArray(arr);
	}
	
	public static void quickSort(int[] arr, int low, int high) {
		
		if(low < high) {		
			int pivot_position = partition(arr, low, high);
			
			quickSort(arr, low,  pivot_position -1);
			quickSort(arr, pivot_position +1, high);
		}
	}
	
	public static int partition(int[] arr, int low, int high) {
		int j = low - 1;
		int pivot = arr[high];
		
		for (int i = low; i <= high; i++) {
			if(arr[i] < pivot) {
				j++;
				//swap arr[j] and arr[i]
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
		
		//swap arr[j+1] and arr[high]
		int temp = arr[high];
		arr[high] = arr[j+1];
		arr[j+1] = temp;
		
		//j+1 will be the right place of the pivot
		return (j+1);	
	}
	
	public static void displayArray(int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(arr[i]);
			}
			else {
				System.out.print(arr[i] + ",");
			}
		
		}
	}

}
